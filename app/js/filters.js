'use strict';

/* Filters */

angular.module('slice', []).filter('slice', function() {
    return function(arr, start, end) {
        return (arr || []).slice(start, end);
    };
});