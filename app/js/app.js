'use strict';

/* App Module */

var homeWork = angular.module('homeWork', [
  'ngRoute',
  'homeControllers',
  'LocalStorageModule'
]);

homeWork.config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('hw_');
  }])

homeWork.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/list', {
        templateUrl: 'view/list.html',
        controller: 'ListCtrl'
      }).
      otherwise({
        redirectTo: '/list'
      });
  }]);
