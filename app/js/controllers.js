'use strict';

/* Controllers */

var homeControllers = angular.module('homeControllers', ['ui.bootstrap', 'slice']);

homeControllers.controller('ListCtrl', ['$scope', '$http', '$modal', 'localStorageService',
  function($scope, $http, $modal, localStorageService) {

    $scope.fields = ["id", "name", "age", "carrier", "snippet"];
    $scope.limits = [
      { label: 5, value: 5 },
      { label: 10, value: 10 },
      { label: 15, value: 15 },
      { label: 20, value: 20 },
      { label: 50, value: 50 }
    ];

    $scope.lists = {};
    $scope.totalItems = 0;
    $scope.column = localStorageService.get('column') || {};
    $scope.order = localStorageService.get('order') || 'id';
    $scope.reverse = localStorageService.get('reverse') === 'true';
    $scope.limit = parseInt(localStorageService.get('limit')) || 5;

    $scope.loadData = function () {
        $http.get('data/list.json').success(function (data) {
          $scope.lists = data;
          $scope.totalItems = data.length;
          $scope.currentPage = parseInt(localStorageService.get('page')) || 1;
        });
    };

    $scope.orderBy = function (field) {
        if ($scope.order != field) {
            $scope.order = field;
        } else {
            $scope.reverse = !$scope.reverse;
        }

        localStorageService.set('order', $scope.order);
        localStorageService.set('reverse', $scope.reverse);
    };

    $scope.editColumns = function () {
        var modalInstance = $modal.open({
            templateUrl: 'view/editcolumns.html',
            controller: 'ColumnsCtrl',
            scope: $scope
        });
        modalInstance.result.then(function (selectedItem) {
            localStorageService.set('column', selectedItem);
        });
    };

    $scope.clickPage = function() {
      localStorageService.set('page', $scope.currentPage );
    };

    $scope.changeLimin = function() {
      localStorageService.set('limit', $scope.limit);
      localStorageService.set('page', 1 );
    };

  }]);

homeControllers.controller('ColumnsCtrl', ['$scope', '$modalInstance',
    function ($scope, $modalInstance) {
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.save = function () {
            $modalInstance.close($scope.column);
        };
    }]);