'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

var selectDropdownbyNum = function ( element, optionNum ) {
  if (optionNum){
    var options = element.findElements(by.tagName('option'))   
      .then(function(options){
        options[optionNum].click();
      });
  }
};

describe('Home work App', function() {

  it('should redirect / to app/#/list', function() {
    browser.get('app/');
    browser.getLocationAbsUrl().then(function(url) {
        expect(url.split('#')[1]).toBe('/list');
      });
  });

  it('should have a title', function() {
    browser.get('app/');
    expect(browser.getTitle()).toEqual('Home Work');
  });


  it('select column', function() {
    browser.get('app/');

    //var fields = element.all(by.repeater('field in fields'));

    element(by.id('edit-column')).click();

    element.all(by.css('input[type="checkbox"]')).click();

  });


  describe('Home work list view', function() {

    beforeEach(function() {
      browser.get('app/#/list');
    });


    it('limit text', function() {

      var list = element.all(by.repeater('item in lists'));
      //var filter = element.all(by.repeater('field in fields'));
      //var limit = element(by.model('limit'));
      //var limit = element(by.model('limit'));

      //element.all(by.css('.limit option:1')).click();

      //expect(limit).toBe(10);


      expect(list.count()).toBe(5);

      element(by.model('limit')).element(by.css('option[value="1"]')).click();

      expect(list.count()).toBe(10);

     });
  });
});
