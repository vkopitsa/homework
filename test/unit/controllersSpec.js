'use strict';

/* jasmine specs for controllers go here */
describe('homeControllers controllers', function() {

  beforeEach(module('homeWork'));

  describe('ListCtrl', function(){
    
    var controller, listScope, ctrl, localStorage;

    beforeEach(inject(function ($controller, $rootScope, $http, $modal, localStorageService) {
      listScope = $rootScope.$new();
      controller = $controller;
      localStorage = localStorageService;

      ctrl = controller('ListCtrl', {
        $scope : listScope,
        $http : $http,
        $modal : $modal,
        localStorageService : localStorage
      });

    }));


    it('table headers should be initialize', function () {
        expect(listScope.lists).toEqual({});
    });

    it('sort', function () {
      listScope.orderBy('id');
      expect(listScope.order).toEqual('id');
      expect(listScope.reverse).toEqual(true);

      listScope.orderBy('id');
      expect(listScope.order).toEqual('id');
      expect(listScope.reverse).toEqual(false);
    });


    it('save page', function () {
      listScope.currentPage = 3;
      listScope.clickPage();

      expect(localStorage.get('page')).toEqual(3);

      listScope.currentPage = 4;
      listScope.clickPage();

      expect(localStorage.get('page')).toEqual(4);

    });

    it('get data', function () {
      httpBackend.when('GET', 'url').respond(response);
      scope.method();
      httpBackend.flush();
    });
    
  });

});
